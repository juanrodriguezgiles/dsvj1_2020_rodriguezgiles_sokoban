#include <iostream>
#include "player.h"
using namespace sokoban;
using namespace controls;
using namespace level;
using namespace screen;
namespace sokoban
{
	namespace player
	{
		Vector2 mousePos;
		Vector2 playerPos;
		Texture2D charup;
		Texture2D chardown;
		Texture2D charleft;
		Texture2D charright;

		void initPlayer()
		{
			mousePos = { 0,0 };
			playerPos = { 0,0 };
			charup = LoadTexture("res/assets/textures/gameplay/charup.png");
			chardown = LoadTexture("res/assets/textures/gameplay/chardown.png");
			charleft = LoadTexture("res/assets/textures/gameplay/charleft.png");
			charright = LoadTexture("res/assets/textures/gameplay/charright.png");
		}
		void updateMousePos()
		{
			mousePos = GetMousePosition();
		}
		//--------------------------------------------------------------------------------
		//Movement
		static void moveBox(int y, int x)
		{
			switch (currentDirection)
			{
			case sokoban::controls::direction::up:
				if (currentLevel[y - 1][x].type == tileType::floor)
				{
					currentLevel[y][x].type = tileType::floor;
					currentLevel[y - 1][x].type = tileType::box;

					currentLevel[(int)playerPos.y - 1][(int)playerPos.x].type = tileType::player;
					currentLevel[(int)playerPos.y][(int)playerPos.x].type = tileType::floor;

					playerPos.y--;
				}
				break;
			case sokoban::controls::direction::down:
				if (currentLevel[y + 1][x].type == tileType::floor)
				{
					currentLevel[y][x].type = tileType::floor;
					currentLevel[y + 1][x].type = tileType::box;

					currentLevel[(int)playerPos.y + 1][(int)playerPos.x].type = tileType::player;
					currentLevel[(int)playerPos.y][(int)playerPos.x].type = tileType::floor;

					playerPos.y++;
				}
				break;
			case sokoban::controls::direction::left:
				if (currentLevel[y][x - 1].type == tileType::floor)
				{
					currentLevel[y][x].type = tileType::floor;
					currentLevel[y][x - 1].type = tileType::box;

					currentLevel[(int)playerPos.y][(int)playerPos.x - 1].type = tileType::player;
					currentLevel[(int)playerPos.y][(int)playerPos.x].type = tileType::floor;

					playerPos.x--;
				}
				break;
			case sokoban::controls::direction::right:
				if (currentLevel[y][x + 1].type == tileType::floor)
				{
					currentLevel[y][x].type = tileType::floor;
					currentLevel[y][x + 1].type = tileType::box;

					currentLevel[(int)playerPos.y][(int)playerPos.x + 1].type = tileType::player;
					currentLevel[(int)playerPos.y][(int)playerPos.x].type = tileType::floor;

					playerPos.x++;
				}
				break;
			case sokoban::controls::direction::noDirection:
			default:
				break;
			}
		}
		static bool isMovementValid()
		{
			switch (currentDirection)
			{
			case sokoban::controls::direction::up:
				return currentLevel[(int)playerPos.y - 1][(int)playerPos.x].type != tileType::wall &&
					!(currentLevel[(int)playerPos.y - 1][(int)playerPos.x].type == tileType::box &&
						currentLevel[(int)playerPos.y - 2][(int)playerPos.x].type == tileType::wall);
				break;
			case sokoban::controls::direction::down:
				return currentLevel[(int)playerPos.y + 1][(int)playerPos.x].type != tileType::wall &&
					!(currentLevel[(int)playerPos.y + 1][(int)playerPos.x].type == tileType::box &&
						currentLevel[(int)playerPos.y + 2][(int)playerPos.x].type == tileType::wall);
				break;
			case sokoban::controls::direction::left:
				return currentLevel[(int)playerPos.y][(int)playerPos.x - 1].type != tileType::wall &&
					!(currentLevel[(int)playerPos.y][(int)playerPos.x - 1].type == tileType::box &&
						currentLevel[(int)playerPos.y][(int)playerPos.x - 2].type == tileType::wall);
				break;
			case sokoban::controls::direction::right:
				return currentLevel[(int)playerPos.y][(int)playerPos.x + 1].type != tileType::wall &&
					!(currentLevel[(int)playerPos.y][(int)playerPos.x + 1].type == tileType::box &&
						currentLevel[(int)playerPos.y][(int)playerPos.x + 2].type == tileType::wall);
				break;
			case sokoban::controls::direction::noDirection:
			default:
				return false;
				break;
			}
		}
		static void exchangeTiles()
		{
			switch (currentDirection)
			{
			case sokoban::controls::direction::up:
				switch (currentLevel[(int)playerPos.y - 1][(int)playerPos.x].type)
				{
				case tileType::floor:
					currentLevel[(int)playerPos.y - 1][(int)playerPos.x].type = tileType::player;
					currentLevel[(int)playerPos.y][(int)playerPos.x].type = tileType::floor;
					playerPos.y--;
					break;
				case tileType::box:
					moveBox((int)playerPos.y - 1, (int)playerPos.x);
					break;
				}
				break;
			case sokoban::controls::direction::down:
				switch (currentLevel[(int)playerPos.y + 1][(int)playerPos.x].type)
				{
				case tileType::floor:
					currentLevel[(int)playerPos.y + 1][(int)playerPos.x].type = tileType::player;
					currentLevel[(int)playerPos.y][(int)playerPos.x].type = tileType::floor;
					playerPos.y++;
					break;
				case tileType::box:
					moveBox((int)playerPos.y + 1, (int)playerPos.x);
					break;
				}
				break;
			case sokoban::controls::direction::left:
				switch (currentLevel[(int)playerPos.y][(int)playerPos.x - 1].type)
				{
				case tileType::floor:
					currentLevel[(int)playerPos.y][(int)playerPos.x - 1].type = tileType::player;
					currentLevel[(int)playerPos.y][(int)playerPos.x].type = tileType::floor;
					playerPos.x--;
					break;
				case tileType::box:
					moveBox((int)playerPos.y, (int)playerPos.x - 1);
					break;
				}
				break;
			case sokoban::controls::direction::right:
				switch (currentLevel[(int)playerPos.y][(int)playerPos.x + 1].type)
				{
				case tileType::floor:
					currentLevel[(int)playerPos.y][(int)playerPos.x + 1].type = tileType::player;
					currentLevel[(int)playerPos.y][(int)playerPos.x].type = tileType::floor;
					playerPos.x++;
					break;
				case tileType::box:
					moveBox((int)playerPos.y, (int)playerPos.x + 1);
					break;
				}
				break;
			default:
				break;
			}
		}
		void move()
		{
			if (isMovementValid())
			{
				exchangeTiles();
			}
		}
		//--------------------------------------------------------------------------------
		//Draw
		void drawPlayer()
		{
			switch (currentDirection)
			{
			case sokoban::controls::direction::noDirection:
				break;
			case sokoban::controls::direction::up:
				DrawTexture(charup, currentLevel[(int)playerPos.y][(int)playerPos.x].body.x, currentLevel[(int)playerPos.y][(int)playerPos.x].body.y, WHITE);
				break;
			case sokoban::controls::direction::down:
				DrawTexture(chardown, currentLevel[(int)playerPos.y][(int)playerPos.x].body.x, currentLevel[(int)playerPos.y][(int)playerPos.x].body.y, WHITE);
				break;
			case sokoban::controls::direction::left:
				DrawTexture(charleft, currentLevel[(int)playerPos.y][(int)playerPos.x].body.x, currentLevel[(int)playerPos.y][(int)playerPos.x].body.y, WHITE);
				break;
			case sokoban::controls::direction::right:
				DrawTexture(charright, currentLevel[(int)playerPos.y][(int)playerPos.x].body.x, currentLevel[(int)playerPos.y][(int)playerPos.x].body.y, WHITE);
				break;
			default:
				break;
			}
		}
		//--------------------------------------------------------------------------------
	}
}