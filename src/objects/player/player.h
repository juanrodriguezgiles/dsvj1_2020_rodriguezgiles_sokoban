#ifndef PLAYER_H
#define PLAYER_H
#include "raylib.h"
#include "game/controls/controls.h"
#include "objects/level/level.h"
namespace sokoban
{
	namespace player
	{
		extern Vector2 mousePos;
		extern Vector2 playerPos;
		extern Texture2D charup;
		extern Texture2D chardown;
		extern Texture2D charleft;
		extern Texture2D charright;

		void initPlayer();
		void updateMousePos();
		void move();
		void drawPlayer();
	}
}
#endif