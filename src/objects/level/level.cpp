#include <iostream>
#include "level.h"
using namespace sokoban;
using namespace player;
using namespace lvl1;
using namespace lvl2;
using namespace lvl3;
using namespace lvl4;
using namespace lvl5;
namespace sokoban
{
	namespace level
	{
		static bool firstRun = true;
		static const int rows = 10;
		static const int columns = 20;
		tile currentLevel[rows][columns];
		levelNumber current;
		static Texture2D present;
		static Texture2D presentGreen;
		static Texture2D floor;
		static Texture2D wall;
		static Texture2D objective;

		void loadLevel()
		{
			clearLevel();

			switch (current)
			{
			case sokoban::level::levelNumber::one:
				loadLevel1();
				break;
			case sokoban::level::levelNumber::two:
				loadLevel2();
				break;
			case sokoban::level::levelNumber::three:
				loadLevel3();
				break;
			case sokoban::level::levelNumber::four:
				loadLevel4();
				break;
			case sokoban::level::levelNumber::five:
				loadLevel5();
				break;
			default:
				break;
			}

			for (int i = 0; i < rows; i++)
			{
				for (int j = 0; j < columns; j++)
				{
					currentLevel[i][j].body.height = (GetScreenHeight() - 50) / rows;
					currentLevel[i][j].body.width = GetScreenWidth() / columns;

					currentLevel[i][j].body.x = currentLevel[i][j].body.width * j;
					currentLevel[i][j].body.y = currentLevel[i][j].body.height * i + 50;

					if (currentLevel[i][j].type == tileType::player)
					{
						playerPos.y = i;
						playerPos.x = j;
					}
				}
			}
			if (firstRun)
			{
				present = LoadTexture("res/assets/textures/gameplay/present.png");
				presentGreen = LoadTexture("res/assets/textures/gameplay/presentGreen.png");
				floor = LoadTexture("res/assets/textures/gameplay/floor.png");
				wall = LoadTexture("res/assets/textures/gameplay/spikes.png");
				objective = LoadTexture("res/assets/textures/gameplay/objective.png");
				present.width = currentLevel[0][0].body.width;
				present.height = currentLevel[0][0].body.height;
				presentGreen.width = currentLevel[0][0].body.width;
				presentGreen.height = currentLevel[0][0].body.height;
				floor.width = currentLevel[0][0].body.width;
				floor.height = currentLevel[0][0].body.height;
				objective.width = currentLevel[0][0].body.width;
				objective.height = currentLevel[0][0].body.height;
				firstRun = false;
			}
		}
		void checkWin()
		{
			for (int i = 0; i < rows; i++)
			{
				for (int j = 0; j < columns; j++)
				{
					if (currentLevel[i][j].objective && currentLevel[i][j].type != tileType::box)
						return;
				}
			}
			sokoban::game::currentScene = sokoban::game::scene::level_complete;
			sokoban::game::init();
		}
		void drawLevel()
		{
			for (int i = 0; i < rows; i++)
			{
				for (int j = 0; j < columns; j++)
				{
					if (currentLevel[i][j].objective)
						DrawTexture(objective, currentLevel[i][j].body.x, currentLevel[i][j].body.y, RAYWHITE);
					switch (currentLevel[i][j].type)
					{
					case tileType::player:
						if (!currentLevel[i][j].objective)
							DrawTexture(floor, currentLevel[i][j].body.x, currentLevel[i][j].body.y, RAYWHITE);
						drawPlayer();
						break;
					case tileType::floor:
						if (!currentLevel[i][j].objective)
							DrawTexture(floor, currentLevel[i][j].body.x, currentLevel[i][j].body.y, RAYWHITE);
						break;
					case tileType::box:
						if (!currentLevel[i][j].objective)
							DrawTexture(floor, currentLevel[i][j].body.x, currentLevel[i][j].body.y, RAYWHITE);
						DrawTexture(present, currentLevel[i][j].body.x, currentLevel[i][j].body.y, RAYWHITE);
						break;
					case tileType::wall:
						DrawTexture(floor, currentLevel[i][j].body.x, currentLevel[i][j].body.y, RAYWHITE);
						DrawTexture(wall, currentLevel[i][j].body.x, currentLevel[i][j].body.y, RAYWHITE);
						break;
					default:
						break;
					}
#if DEBUG
					DrawRectangleLinesEx(currentLevel[i][j].body, 2, GREEN);
#endif
				}
			}
		}
		void clearLevel()
		{
			for (int i = 0; i < rows; i++)
			{
				for (int j = 0; j < columns; j++)
				{
					currentLevel[i][j].type = tileType::empty;
					currentLevel[i][j].objective = false;
				}
			}
		}
		void unLoadTextures()
		{
			UnloadTexture(present);
			UnloadTexture(presentGreen);
			UnloadTexture(floor);
			UnloadTexture(wall);
			UnloadTexture(objective);
		}
	}
}