#ifndef LEVEL_H
#define LEVEL_H
#include "raylib.h"
#include "game/game.h"
#include "objects/player/player.h"
#include "levels/lvl1/lvl1.h"
#include "levels/lvl2/lvl2.h"
#include "levels/lvl3/lvl3.h"
#include "levels/lvl4/lvl4.h"
#include "levels/lvl5/lvl5.h"
namespace sokoban
{
	namespace level
	{
		enum class levelNumber
		{
			one=1,
			two,
			three,
			four,
			five
		};
		enum class tileType
		{
			empty,
			wall,
			floor,
			box,
			player
		};
		struct tile
		{
			Rectangle body;
			tileType type = tileType::empty;
			Texture2D texture;
			bool active = false;
			bool objective = false;
		};
		extern tile currentLevel[10][20];
		extern levelNumber current;

		void loadLevel();
		void checkWin();
		void drawLevel();
		void clearLevel();
		void unLoadTextures();
	}
}
#endif