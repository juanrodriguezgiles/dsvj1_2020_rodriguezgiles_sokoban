#include "lvl2.h"
using namespace sokoban;
using namespace level;
namespace sokoban
{
	namespace lvl2
	{
		void loadLevel2()
		{
			currentLevel[2][7].type = tileType::floor;
			currentLevel[2][8].type = tileType::floor; currentLevel[2][8].objective = true;

			currentLevel[3][7].type = tileType::floor;
			currentLevel[3][8].type = tileType::floor;

			currentLevel[4][7].type = tileType::floor;
			currentLevel[4][8].type = tileType::player;
			currentLevel[4][9].type = tileType::floor;
			currentLevel[4][10].type = tileType::floor;

			currentLevel[5][7].type = tileType::floor;
			currentLevel[5][8].type = tileType::floor;
			currentLevel[5][9].type = tileType::box;
			currentLevel[5][10].type = tileType::floor;

			currentLevel[6][7].type = tileType::floor;
			currentLevel[6][8].type = tileType::floor;
		}
	}
}