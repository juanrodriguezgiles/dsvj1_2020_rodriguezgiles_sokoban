#ifndef LVL2_H
#define LVL2_H
#include "raylib.h"
#include "objects/level/level.h"
namespace sokoban
{
	namespace lvl2
	{
		void loadLevel2();
	}
}
#endif
