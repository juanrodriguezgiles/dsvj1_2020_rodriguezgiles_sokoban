#ifndef LVL4_H
#define LVL4_H
#include "raylib.h"
#include "objects/level/level.h"
namespace sokoban
{
	namespace lvl4
	{
		void loadLevel4();
	}
}
#endif