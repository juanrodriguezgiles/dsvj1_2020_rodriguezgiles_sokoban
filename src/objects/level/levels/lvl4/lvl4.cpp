#include "lvl4.h"
using namespace sokoban;
using namespace level;
namespace sokoban
{
	namespace lvl4
	{
		void loadLevel4()
		{
			currentLevel[2][9].type = tileType::floor;
			currentLevel[2][10].type = tileType::floor;

			currentLevel[3][8].type = tileType::player;
			currentLevel[3][9].type = tileType::box;
			currentLevel[3][10].type = tileType::floor; currentLevel[3][10].objective = true;

			currentLevel[4][7].type = tileType::floor;
			currentLevel[4][8].type = tileType::box;
			currentLevel[4][9].type = tileType::box;
			currentLevel[4][10].type = tileType::floor;
			currentLevel[4][11].type = tileType::floor;

			currentLevel[5][7].type = tileType::floor;
			currentLevel[5][8].type = tileType::floor; currentLevel[5][8].objective = true;
			currentLevel[5][9].type = tileType::floor;
			currentLevel[5][10].type = tileType::floor; currentLevel[5][10].objective = true;
			currentLevel[5][11].type = tileType::floor;

			currentLevel[6][9].type = tileType::floor;
			currentLevel[6][10].type = tileType::floor;
			currentLevel[6][11].type = tileType::floor;
		}
	}
}