#include "lvl3.h"
using namespace sokoban;
using namespace level;
namespace sokoban
{
	namespace lvl3
	{
		void loadLevel3()
		{
			currentLevel[2][7].type = tileType::floor;
			currentLevel[2][8].type = tileType::player;
			currentLevel[2][9].type = tileType::floor;

			currentLevel[3][7].type = tileType::floor; currentLevel[3][7].objective = true;
			currentLevel[3][8].type = tileType::floor; currentLevel[3][8].objective = true;
			currentLevel[3][9].type = tileType::floor; currentLevel[3][9].objective = true;

			currentLevel[4][7].type = tileType::box;
			currentLevel[4][8].type = tileType::box;
			currentLevel[4][9].type = tileType::box;

			currentLevel[5][7].type = tileType::floor;
			currentLevel[5][8].type = tileType::floor;
			currentLevel[5][9].type = tileType::floor;
			currentLevel[5][10].type = tileType::floor;


			currentLevel[6][7].type = tileType::floor;
			currentLevel[6][8].type = tileType::floor;
			currentLevel[6][9].type = tileType::floor;
			currentLevel[6][10].type = tileType::floor;

			currentLevel[7][7].type = tileType::floor;
			currentLevel[7][8].type = tileType::floor;
			currentLevel[7][9].type = tileType::floor;
			currentLevel[7][10].type = tileType::floor;
		}
	}
}