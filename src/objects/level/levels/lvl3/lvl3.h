#ifndef LVL3_H
#define LVL3_H
#include "raylib.h"
#include "objects/level/level.h"
namespace sokoban
{
	namespace lvl3
	{
		void loadLevel3();
	}
}
#endif