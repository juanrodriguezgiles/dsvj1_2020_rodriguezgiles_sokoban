#ifndef LVL5_H
#define LVL5_H
#include "raylib.h"
#include "objects/level/level.h"
namespace sokoban
{
	namespace lvl5
	{
		void loadLevel5();
	}
}
#endif