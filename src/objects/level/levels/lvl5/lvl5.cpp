#include "lvl5.h"
using namespace sokoban;
using namespace level;
namespace sokoban
{
	namespace lvl5
	{
		void loadLevel5()
		{
			currentLevel[2][7].type = tileType::floor; currentLevel[2][7].objective = true;
			currentLevel[2][8].type = tileType::floor;
			currentLevel[2][9].type = tileType::wall;
			currentLevel[2][10].type = tileType::floor;
			currentLevel[2][11].type = tileType::floor;

			currentLevel[3][7].type = tileType::floor;
			currentLevel[3][8].type = tileType::floor;
			currentLevel[3][9].type = tileType::box;
			currentLevel[3][10].type = tileType::floor;
			currentLevel[3][11].type = tileType::floor;

			currentLevel[4][7].type = tileType::floor; currentLevel[4][7].objective = true;
			currentLevel[4][8].type = tileType::floor;
			currentLevel[4][9].type = tileType::box;
			currentLevel[4][10].type = tileType::wall;
			currentLevel[4][11].type = tileType::player;

			currentLevel[5][7].type = tileType::floor;
			currentLevel[5][8].type = tileType::floor;
			currentLevel[5][9].type = tileType::box;
			currentLevel[5][10].type = tileType::floor;
			currentLevel[5][11].type = tileType::floor;

			currentLevel[6][7].type = tileType::floor; currentLevel[6][7].objective = true;
			currentLevel[6][8].type = tileType::floor;
			currentLevel[6][9].type = tileType::wall;
			currentLevel[6][10].type = tileType::floor;
			currentLevel[6][11].type = tileType::floor;
		}
	}
}