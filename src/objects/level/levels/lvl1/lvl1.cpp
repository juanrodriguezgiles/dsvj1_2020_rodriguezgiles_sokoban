#include "lvl1.h"
using namespace sokoban;
using namespace level;
namespace sokoban
{
	namespace lvl1
	{
		void loadLevel1()
		{
			currentLevel[3][8].type = tileType::floor; currentLevel[3][8].objective = true;
			currentLevel[3][9].type = tileType::floor;
			currentLevel[3][10].type = tileType::floor;
			currentLevel[3][11].type = tileType::floor; currentLevel[3][11].objective = true;

			currentLevel[4][8].type = tileType::player;
			currentLevel[4][9].type = tileType::wall;
			currentLevel[4][10].type = tileType::wall;
			currentLevel[4][11].type = tileType::floor;

			currentLevel[5][8].type = tileType::floor;
			currentLevel[5][9].type = tileType::wall;
			currentLevel[5][10].type = tileType::wall;
			currentLevel[5][11].type = tileType::floor;

			currentLevel[6][8].type = tileType::floor;
			currentLevel[6][9].type = tileType::box;
			currentLevel[6][10].type = tileType::floor;
			currentLevel[6][11].type = tileType::box;

			currentLevel[7][8].type = tileType::floor;
			currentLevel[7][9].type = tileType::floor;
			currentLevel[7][10].type = tileType::floor;
			currentLevel[7][11].type = tileType::floor;
		}
	}
}