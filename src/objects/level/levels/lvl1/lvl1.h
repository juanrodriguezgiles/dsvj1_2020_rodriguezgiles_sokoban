#ifndef LVL1_H
#define LVL1_H
#include "raylib.h"
#include "objects/level/level.h"
namespace sokoban
{
	namespace lvl1
	{
		void loadLevel1();
	}
}
#endif
