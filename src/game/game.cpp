#include "game.h"
#include "raylib.h"
namespace sokoban
{
	namespace game
	{
		scene currentScene = scene::main_menu;
		bool playing = true;
		double time;

		void init()
		{
			switch (currentScene)
			{
			case sokoban::game::scene::main_menu:
				sokoban::main_menu::init();
				break;
			case sokoban::game::scene::credits:
				sokoban::credits::init();
				break;
			case sokoban::game::scene::gameplay:
				sokoban::gameplay::init();
				break;
			case sokoban::game::scene::pause:
				sokoban::pause::init();
				break;
			case sokoban::game::scene::exitGame:
				sokoban::exitGame::init();
				break;
			case sokoban::game::scene::how_to_play:
				sokoban::how_to_play::init();
				break;
			case sokoban::game::scene::level_select:
				sokoban::level_select::init();
				break;
			case sokoban::game::scene::level_complete:
				sokoban::level_complete::init();
				break;
			default:
				break;
			}
		}
		static void input()
		{
			switch (currentScene)
			{
			case sokoban::game::scene::main_menu:
				sokoban::main_menu::input();
				break;
			case sokoban::game::scene::credits:
				sokoban::credits::input();
				break;
			case sokoban::game::scene::gameplay:
				sokoban::gameplay::input();
				break;
			case sokoban::game::scene::pause:
				sokoban::pause::input();
				break;
			case sokoban::game::scene::exitGame:
				sokoban::exitGame::input();
				break;
			case sokoban::game::scene::how_to_play:
				sokoban::how_to_play::input();
				break;
			case sokoban::game::scene::level_select:
				sokoban::level_select::input();
				break;
			case sokoban::game::scene::level_complete:
				sokoban::level_complete::input();
				break;
			default:
				break;
			}
		}
		static void update()
		{
			switch (currentScene)
			{
			case sokoban::game::scene::main_menu:
				sokoban::main_menu::update();
				break;
			case sokoban::game::scene::credits:
				sokoban::credits::update();
				break;
			case sokoban::game::scene::gameplay:
				sokoban::gameplay::update();
				break;
			case sokoban::game::scene::pause:
				sokoban::pause::update();
				break;
			case sokoban::game::scene::exitGame:
				sokoban::exitGame::update();
				break;
			case sokoban::game::scene::how_to_play:
				sokoban::how_to_play::update();
				break;
			case sokoban::game::scene::level_select:
				sokoban::level_select::update();
				break;
			case sokoban::game::scene::level_complete:
				sokoban::level_complete::update();
				break;
			default:
				break;
			}
		}
		static void draw()
		{
			BeginDrawing();
			ClearBackground(BLACK);
			switch (currentScene)
			{
			case sokoban::game::scene::main_menu:
				sokoban::main_menu::draw();
				break;
			case sokoban::game::scene::credits:
				sokoban::credits::draw();
				break;
			case sokoban::game::scene::gameplay:
				sokoban::gameplay::draw();
				break;
			case sokoban::game::scene::pause:
				sokoban::pause::draw();
				break;
			case sokoban::game::scene::exitGame:
				sokoban::exitGame::draw();
				break;
			case sokoban::game::scene::how_to_play:
				sokoban::how_to_play::draw();
				break;
			case sokoban::game::scene::level_select:
				sokoban::level_select::draw();
				break;
			case sokoban::game::scene::level_complete:
				sokoban::level_complete::draw();
				break;
			default:
				break;
			}
			EndDrawing();
		}
		static void deInit()
		{
			level::unLoadTextures();
			credits::unLoadTextures();
			exitGame::unLoadTextures();
			how_to_play::unLoadTextures();
			level_complete::unLoadTextures();
			level_select::unLoadTextures();
			main_menu::unLoadTextures();
			pause::unLoadTextures();
			UnloadTexture(screen::backgroundGameplay);
			UnloadTexture(screen::backgroundMainMenu);
			UnloadTexture(player::charup);
			UnloadTexture(player::chardown);
			UnloadTexture(player::charleft);
			UnloadTexture(player::charright);
			UnloadMusicStream(gameplay::gameplayMusic);
			UnloadMusicStream(main_menu::menuMusic);
			UnloadSound(main_menu::click);
			CloseAudioDevice();
			CloseWindow();
		}
		void run()
		{
			while (!WindowShouldClose() && playing)
			{
				input();
				update();
				draw();
			}
			deInit();
		}
	}
}