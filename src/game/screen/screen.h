#ifndef SCREEN_H
#define SCREEN_H
#include "raylib.h"
#include "objects/player/player.h"
namespace sokoban
{
	namespace screen
	{
		extern const int screenWidth;
		extern const int screenHeight;
		extern const int screenMiddleX;
		extern const int screenMiddleY;
		extern const int textSpacing;
		const extern int framesSpeed;
		extern int framesCounter;
		extern int currentFrame;
		extern Rectangle frameRec;
		extern Texture2D backgroundGameplay;
		extern Texture2D backgroundMainMenu;

		void initScreen();
		void drawBackground();
		void updateFrames();
	}
}
#endif