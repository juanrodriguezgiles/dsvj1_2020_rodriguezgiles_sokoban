#include "screen.h"
using namespace sokoban;
using namespace player;
using namespace controls;
namespace sokoban
{
	namespace screen
	{
		const int screenWidth = 1200;
		const int screenHeight = 800;
		const int screenMiddleX = screenWidth / 2;
		const int screenMiddleY = screenHeight / 2;
		const int textSpacing = 100;
		const int framesSpeed = 8;
		int framesCounter;
		int currentFrame;
		Rectangle frameRec;
		Texture2D backgroundGameplay;
		Texture2D backgroundMainMenu;

		void initScreen()
		{
			framesCounter = 0;
			currentFrame = 0;
			backgroundGameplay = LoadTexture("res/assets/textures/gameplay/backgroundGameplay.png");
			backgroundMainMenu = LoadTexture("res/assets/textures/mainMenu/backgroundMainMenu.png");
			backgroundMainMenu.width = screenWidth;
			backgroundMainMenu.height = screenHeight;
			backgroundGameplay.width = screenWidth;
			backgroundGameplay.height = screenHeight;
		}
		void drawBackground()
		{
			DrawTexture(backgroundGameplay, 0, 0, WHITE);
		}
		void updateFrames()
		{
			//framesCounter++;

			//if (framesCounter >= (60 / framesSpeed))
			//{
			//	framesCounter = 0;
			//	currentFrame++;

			//	if (currentFrame > 4) currentFrame = 0;

			//	frameRec.y = (float)currentFrame * (float)character.height / 4;
			//}
		}
	}
}