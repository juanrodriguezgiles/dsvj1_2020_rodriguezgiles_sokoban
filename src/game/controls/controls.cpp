#include "controls.h"
namespace sokoban
{
	namespace controls
	{
		const int up = KEY_UP;
		const int down = KEY_DOWN;
		const int left = KEY_LEFT;
		const int right = KEY_RIGHT;
		const int pauseGame = KEY_P;

		direction currentDirection = direction::down;
	}
}