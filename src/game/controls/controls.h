#ifndef CONTROLS_H
#define CONTROLS_H
#include "raylib.h"
namespace sokoban
{
	namespace controls
	{
		enum class direction
		{
			noDirection,
			up,
			down,
			left,
			right
		};
		extern const int up;
		extern const int down;
		extern const int left;
		extern const int right;
		extern const int pauseGame;
		extern direction currentDirection;
	}
}
#endif
