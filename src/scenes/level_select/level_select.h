#ifndef LEVEL_SELECT_H
#define LEVEL_SELECT_H
#include "raylib.h"
#include "game/game.h"
#include "game/screen/screen.h"
namespace sokoban
{
	namespace level_select
	{
		void init();
		void input();
		void update();
		void draw();
		void unLoadTextures();
	}
}
#endif