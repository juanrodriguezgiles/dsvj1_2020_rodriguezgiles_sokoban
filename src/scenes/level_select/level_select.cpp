#include "level_select.h"
using namespace sokoban;
using namespace game;
using namespace screen;
using namespace player;
using namespace main_menu;
using namespace level;
namespace sokoban
{
	namespace level_select
	{
		static bool firstRun = true;
		static Texture2D levelSelectPanel;
		static Texture2D present;
		static Texture2D presentOpen;
		static Rectangle level[5];
		void init()
		{
			if (firstRun)
			{
				levelSelectPanel = LoadTexture("res/assets/textures/level_select/panel.png");
				present = LoadTexture("res/assets/textures/level_select/pres1.png");
				presentOpen = LoadTexture("res/assets/textures/level_select/pres1open.png");
				for (int i = 0; i < 5; i++)
				{
					level[i] = { (float)screenMiddleX - 210 + 85 * i,(float)screenMiddleY + 20,(float)present.width,(float)present.height };
				}
				firstRun = false;
			}
		}
		void input()
		{
			for (int i = 0; i < 5; i++)
			{
				if (CheckCollisionPointRec(mousePos, level[i]) && IsMouseButtonDown(MOUSE_LEFT_BUTTON))
				{
					PlaySound(click);
					current = levelNumber(i + 1);
					currentScene = scene::gameplay;
					game::init();
				}
			}
		}
		void update()
		{
			updateMousePos();
			UpdateMusicStream(menuMusic);
		}
		void draw()
		{
			DrawTexture(backgroundMainMenu, 0, 0, WHITE);
			DrawTexture(levelSelectPanel, screenMiddleX - levelSelectPanel.width / 2, screenMiddleY - levelSelectPanel.height / 2, WHITE);
			for (int i = 0; i < 5; i++)
			{
				if (CheckCollisionPointRec(mousePos, level[i]))
					DrawTexture(presentOpen, level[i].x, level[i].y, WHITE);
				else
					DrawTexture(present, level[i].x, level[i].y, WHITE);
			}
		}
		void unLoadTextures()
		{
			UnloadTexture(levelSelectPanel);
			UnloadTexture(present);
			UnloadTexture(presentOpen);
		}
	}
}