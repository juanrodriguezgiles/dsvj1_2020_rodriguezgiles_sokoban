#ifndef LEVEL_COMPLETE_H
#define LEVEL_COMPLETE_H
#include "raylib.h"
#include "game/game.h"
#include "game/screen/screen.h"
namespace sokoban
{
	namespace level_complete
	{
		void init();
		void input();
		void update();
		void draw();
		void unLoadTextures();
	}
}
#endif