#include "level_complete.h"
using namespace sokoban;
using namespace game;
using namespace screen;
using namespace player;
using namespace gameplay;
using namespace main_menu;
using namespace level;
namespace sokoban
{
	namespace level_complete
	{
		static bool firstRun = true;
		static Texture2D panel;
		static Texture2D mainMenu;
		static Texture2D mainMenuH;
		static Texture2D nextLevel;
		static Texture2D nextLevelH;
		static Rectangle mainMenuR;
		static Rectangle nextLevelR;

		void init()
		{
			if (firstRun)
			{
				panel = LoadTexture("res/assets/textures/level_complete/panel.png");
				mainMenu = LoadTexture("res/assets/textures/level_complete/mainMenu.png");
				mainMenuH = LoadTexture("res/assets/textures/level_complete/mainMenuH.png");
				nextLevel = LoadTexture("res/assets/textures/level_complete/nextLevel.png");
				nextLevelH = LoadTexture("res/assets/textures/level_complete/nextLevelH.png");
				mainMenuR = { (float)screenMiddleX - mainMenu.width * 2 + 210,(float)screenMiddleY - 50,(float)mainMenu.width,(float)mainMenu.height };
				nextLevelR = { (float)screenMiddleX - nextLevel.width * 2 + 140,(float)screenMiddleY + 60,(float)nextLevel.width,(float)nextLevel.height };
			}
		}
		void input()
		{
			if (CheckCollisionPointRec(mousePos, mainMenuR) && IsMouseButtonDown(MOUSE_LEFT_BUTTON))
			{
				PlaySound(click);
				currentScene = scene::main_menu;
				time = GetTime();
				game::init();
			}
			if (CheckCollisionPointRec(mousePos, nextLevelR) && IsMouseButtonDown(MOUSE_LEFT_BUTTON) && current != levelNumber::five)
			{
				PlaySound(click);
				switch (current)
				{
				case sokoban::level::levelNumber::one:
					current = levelNumber::two;
					break;
				case sokoban::level::levelNumber::two:
					current = levelNumber::three;
					break;
				case sokoban::level::levelNumber::three:
					current = levelNumber::four;
					break;
				case sokoban::level::levelNumber::four:
					current = levelNumber::five;
					break;
				default:
					break;
				}
				currentScene = scene::gameplay;
				game::init();
			}
		}
		void update()
		{
			updateMousePos();
			UpdateMusicStream(gameplayMusic);
		}
		void draw()
		{
			DrawTexture(backgroundMainMenu, 0, 0, WHITE);
			DrawTexture(panel, screenMiddleX - panel.width, screenMiddleY - panel.height / 2, WHITE);
			if (CheckCollisionPointRec(mousePos, mainMenuR))
				DrawTexture(mainMenuH, (int)mainMenuR.x, (int)mainMenuR.y, WHITE);
			else
				DrawTexture(mainMenu, (int)mainMenuR.x, (int)mainMenuR.y, WHITE);
			if (CheckCollisionPointRec(mousePos, nextLevelR) && current != levelNumber::five)
				DrawTexture(nextLevelH, (int)nextLevelR.x, (int)nextLevelR.y, WHITE);
			else if (current != levelNumber::five)
				DrawTexture(nextLevel, (int)nextLevelR.x, (int)nextLevelR.y, WHITE);
		}
		void unLoadTextures()
		{
			UnloadTexture(panel);
			UnloadTexture(mainMenu);
			UnloadTexture(mainMenuH);
			UnloadTexture(nextLevel);
			UnloadTexture(nextLevelH);
		}
	}
}