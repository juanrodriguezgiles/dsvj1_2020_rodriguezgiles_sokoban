#ifndef HOW_TO_PLAY_H
#define HOW_TO_PLAY_H
#include "raylib.h"
#include "game/game.h"
namespace sokoban
{
	namespace how_to_play
	{
		void init();
		void input();
		void update();
		void draw();
		void unLoadTextures();
	}
}
#endif