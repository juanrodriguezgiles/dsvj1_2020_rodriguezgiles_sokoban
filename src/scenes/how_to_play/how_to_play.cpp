#include "how_to_play.h"
using namespace sokoban;
using namespace game;
using namespace screen;
using namespace player;
using namespace main_menu;
namespace sokoban
{
	namespace how_to_play
	{
		static bool firstRun = true;
		static Texture2D tutorial;
		static Texture2D mainMenu;
		static Texture2D mainMenuH;
		static Rectangle mainMenuRec;
		void init()
		{
			if (firstRun)
			{
				tutorial = LoadTexture("res/assets/textures/howToPlay/howTo.png");
				mainMenu = LoadTexture("res/assets/textures/level_complete/mainMenu.png");
				mainMenuH = LoadTexture("res/assets/textures/level_complete/mainMenuH.png");
				mainMenuRec = { (float)screenMiddleX - mainMenu.width / 2 + 30,(float)screenMiddleY + 300,(float)mainMenu.width,(float)mainMenu.height };
				firstRun = false;
			}
		}
		void input()
		{
			if (CheckCollisionPointRec(mousePos, mainMenuRec) && IsMouseButtonDown(MOUSE_LEFT_BUTTON))
			{
				PlaySound(click);
				currentScene = scene::main_menu;
				game::init();
			}
		}
		void update()
		{
			updateMousePos();
			UpdateMusicStream(menuMusic);
		}
		void draw()
		{
			DrawTexture(tutorial, 0, 0, WHITE);
			if (CheckCollisionPointRec(mousePos, mainMenuRec))
				DrawTexture(mainMenuH, (int)mainMenuRec.x, (int)mainMenuRec.y, WHITE);
			else
				DrawTexture(mainMenu, (int)mainMenuRec.x, (int)mainMenuRec.y, WHITE);
		}
		void unLoadTextures()
		{
			UnloadTexture(tutorial);
			UnloadTexture(mainMenu);
			UnloadTexture(mainMenuH);
		}
	}
}