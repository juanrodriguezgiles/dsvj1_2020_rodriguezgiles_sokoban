#include "credits.h"
using namespace sokoban;
using namespace game;
using namespace screen;
using namespace main_menu;
using namespace player;
namespace sokoban
{
	namespace credits
	{
		static bool firstRun = true;
		static Texture2D credits;
		static Texture2D mainMenu;
		static Texture2D mainMenuH;
		static Rectangle mainMenuRec;
		void init()
		{
			if (firstRun)
			{
				credits = LoadTexture("res/assets/textures/credits/credits.png");
				mainMenu = LoadTexture("res/assets/textures/credits/mainMenu.png");
				mainMenuH = LoadTexture("res/assets/textures/credits/mainMenuH.png");
				mainMenuRec = { (float)screenMiddleX - mainMenu.width / 2 + 30,(float)screenMiddleY + 300,(float)mainMenu.width,(float)mainMenu.height };
				firstRun = false;
			}
		}
		void input()
		{
			if (CheckCollisionPointRec(mousePos, mainMenuRec) && IsMouseButtonDown(MOUSE_LEFT_BUTTON))
			{
				PlaySound(click);
				currentScene = scene::main_menu;
				game::init();
			}
		}
		void update()
		{
			updateMousePos();
			UpdateMusicStream(menuMusic);
		}
		void draw()
		{
			DrawTexture(credits, 0, 0, WHITE);
			if (CheckCollisionPointRec(mousePos, mainMenuRec))
				DrawTexture(mainMenuH, (int)mainMenuRec.x, (int)mainMenuRec.y, WHITE);
			else
				DrawTexture(mainMenu, (int)mainMenuRec.x, (int)mainMenuRec.y, WHITE);
		}
		void unLoadTextures()
		{
			UnloadTexture(credits);
			UnloadTexture(mainMenu);
			UnloadTexture(mainMenuH);
		}
	}
}