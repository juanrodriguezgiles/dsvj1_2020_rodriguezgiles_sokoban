#ifndef CREDITS_H
#define CREDITS_H
#include "raylib.h"
#include "game/game.h"
namespace sokoban
{
	namespace credits
	{
		void init();
		void input();
		void update();
		void draw();
		void unLoadTextures();
	}
}
#endif
