#ifndef PAUSE_H
#define PAUSE_H
#include "raylib.h"
#include "game/game.h"
namespace sokoban
{
	namespace pause
	{
		void init();
		void input();
		void update();
		void draw();
		void unLoadTextures();
	}
}
#endif