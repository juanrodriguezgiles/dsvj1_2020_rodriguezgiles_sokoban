#include "pause.h"
using namespace sokoban;
using namespace game;
using namespace screen;
using namespace player;
using namespace gameplay;
using namespace main_menu;
using namespace level;
namespace sokoban
{
	namespace pause
	{
		static bool firstRun = true;
		static Texture2D pausePanel;

		void init()
		{
			if (firstRun)
			{
				pausePanel = LoadTexture("res/assets/textures/pause/pause.png");
				firstRun = false;
			}
		}
		void input()
		{
			if (IsKeyPressed(KEY_P))
			{
				PlaySound(click);
				currentScene = scene::gameplay;
			}
			if (IsKeyPressed(KEY_M))
			{
				PlaySound(click);
				currentScene = scene::main_menu;
				game::init();
			}
			if (IsKeyPressed(KEY_R))
			{
				PlaySound(click);
				currentScene = scene::gameplay;
				game::init();
			}
		}
		void update()
		{
			updateMousePos();
			UpdateMusicStream(gameplayMusic);
		}
		void draw()
		{
			DrawTexture(backgroundGameplay, 0, 0, WHITE);
			drawLevel();
			DrawTexture(pausePanel, (int)screenMiddleX - pausePanel.width / 2, (int)screenMiddleY - pausePanel.height / 2, WHITE);
		}
		void unLoadTextures()
		{
			UnloadTexture(pausePanel);
		}
	}
}