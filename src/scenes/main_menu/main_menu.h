#ifndef MAIN_MENU_H
#define MAIN_MENU_H
#include "raylib.h"
#include "game/game.h"
#include "game/screen/screen.h"
#include "objects/player/player.h"
namespace sokoban
{
	namespace main_menu
	{
		extern Sound click;
		extern Music menuMusic;

		void init();
		void input();
		void update();
		void draw();
		void unLoadTextures();
	}
}
#endif
