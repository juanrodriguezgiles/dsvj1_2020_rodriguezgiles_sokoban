#ifndef EXIT_H
#define EXIT_H
#include "raylib.h"
#include "game/game.h"
namespace sokoban
{
	namespace exitGame
	{
		void init();
		void input();
		void update();
		void draw();
		void unLoadTextures();
	}
}
#endif