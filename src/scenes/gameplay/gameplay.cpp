#include <iostream>
#include "gameplay.h"
using namespace sokoban;
using namespace game;
using namespace controls;
using namespace screen;
using namespace player;
using namespace level;
using namespace main_menu;
namespace sokoban
{
	namespace gameplay
	{
		static bool firstRun = true;
		Music gameplayMusic;

		void init()
		{
			if (firstRun)
			{
				gameplayMusic = LoadMusicStream("res/assets/sounds/gamePlay.ogg");
				PlayMusicStream(gameplayMusic);
				SetMusicVolume(gameplayMusic, 0.06f);
				firstRun = false;
			}
			initPlayer();
			loadLevel();
		}
		void input()
		{
			if (IsKeyPressed(pauseGame))
			{
				PlaySound(click);
				currentScene = scene::pause;
				game::init();
			}
			if (IsKeyReleased(up))
			{
				currentDirection = direction::up;
				move();
			}
			if (IsKeyReleased(down))
			{
				currentDirection = direction::down;
				move();
			}
			if (IsKeyReleased(left))
			{
				currentDirection = direction::left;
				move();
			}
			if (IsKeyReleased(right))
			{
				currentDirection = direction::right;
				move();
			}
		}
		void update()
		{
			checkWin();
			updateFrames();
			updateMousePos();
			UpdateMusicStream(gameplayMusic);
		}
		void draw()
		{
			DrawTexture(backgroundMainMenu, 0, 0, WHITE);
			drawLevel();
		}
	}
}