#ifndef GAMEPLAY_H
#define GAMEPLAY_H
#include "raylib.h"
#include "game/game.h"
#include "game/controls/controls.h"
#include "objects/level/level.h"
namespace sokoban
{
	namespace gameplay
	{
		extern Music gameplayMusic;
		void init();
		void input();
		void update();
		void draw();
	}
}
#endif